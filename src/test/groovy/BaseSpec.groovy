import spock.lang.*

abstract class BaseSpec extends Specification {
    def baseField = { println 'BaseField initializer' }()

    def setupSpec() { println 'setupSpec()' }
    def cleanupSpec() { println 'cleanupSpec()' }

    def setup() { println 'setup()' }
    def cleanup() { println 'cleanup()' }
}

