import Calculator
import BaseSpec


class SpockDemo extends BaseSpec {

    def "use given when then blocks"() {
        println "Start test: '$specificationContext.currentIteration.name'"

        given:
        int a = 1
        int b = 1

        when:
        int res = a + b

        then:
        res == 2

        println "Finish: '$specificationContext.currentIteration.name'"
    }

    def "use expect (alternative given when then)"() {
        println "Start test: '$specificationContext.currentIteration.name'"

        expect:
        1 + 1 == 2

        println "Finish: '$specificationContext.currentIteration.name'"
    }

    def "use groovy list"() {
        println "Start test: '$specificationContext.currentIteration.name'"

        given:
        def list = [1, 2, 3, 4]

        when:
        list.remove(0)

        then:
        list == [2, 3, 4] // Оператор "==" тоже самое, что и equals()

        println "Finish: '$specificationContext.currentIteration.name'"
    }

    def "use thrown for catching exception"() {
        println "Start test: '$specificationContext.currentIteration.name'"

        given:
        def list = [1, 2, 3, 4]

        when:
        list.remove(20)

        then:
        thrown(IndexOutOfBoundsException)
        list.size() == 4

        println "Finish: '$specificationContext.currentIteration.name'"
    }

    def "use datatables (parameterized test)"(int a, int b, int c) {
        println "Start test: '$specificationContext.currentIteration.name'"

        expect:
        Math.pow(a, b) == c

        println "Finish: '$specificationContext.currentIteration.name'"

        where:
        a | b | c
        1 | 2 | 1
        2 | 2 | 4
        3 | 2 | 9
    }

    def "use mock"() {
        println "Start test: '$specificationContext.currentIteration.name'"

        given:
        def calculator_mock = Mock(Calculator)

        when:
        def res = calculator_mock.addition(1, 1) // Вернет 0.0 по умолчанию

        then:
        !res // 0.0 == False

        println "Finish: '$specificationContext.currentIteration.name'"
    }

    def "use mock with behavior change"() {
        println "Start test: '$specificationContext.currentIteration.name'"

        given:
        def calculator_mock = Mock(Calculator)
        calculator_mock.addition(1, 1) >> 2

        when:
        def res = calculator_mock.addition(1, 1) // Вернет 2

        then:
        res == 2

        println "Finish: '$specificationContext.currentIteration.name'"
    }

    def "use mock with behavior change list"() {
        println "Start test: '$specificationContext.currentIteration.name'"

        given:
        def calculator_mock = Mock(Calculator)
        def results_list = [1, 4, 6]
        calculator_mock.addition(_, _) >>> results_list

        when:
        def res1 = calculator_mock.addition(1,1) // results_list[0]
        def res2 = calculator_mock.addition(2,2) // results_list[1]
        def res3 = calculator_mock.addition(2,2) // results_list[2]
        def res4 = calculator_mock.addition(3,3) // results_list[2]

        then:
        res1 == 1
        res2 == 4
        res3 == 6
        res4 == 6

        println "Finish: '$specificationContext.currentIteration.name'"
    }

}
