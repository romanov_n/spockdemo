﻿Для запуска демо версии Spock нужно:

1) Зазрузить необходимые зависимости
2) Запустить тесты

!!! Внимание !!!
Если будут ошибки с версией Java при запуске тестов, то стоит поправить версию в pom.xml на вашу + в настройках самого проекта.
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>11</source> // YOUR SDK VERSION
                    <target>11</target> // YOUR SDK VERSION
                </configuration>
            </plugin>
        </plugins>
    </build>